(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.2' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     26099,        759]
NotebookOptionsPosition[     22878,        697]
NotebookOutlinePosition[     23225,        712]
CellTagsIndexPosition[     23182,        709]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["Just to do an expansion of coefficients", "Title",
 CellChangeTimes->{{3.8393089367783365`*^9, 
  3.8393089524037495`*^9}},ExpressionUUID->"c0d3e94b-b71f-4610-8faa-\
14a51e2f4819"],

Cell[BoxData[
 RowBox[{
  RowBox[{"u\[Beta]", "[", 
   RowBox[{"T_", ",", " ", "\[Omega]_"}], "]"}], ":=", 
  RowBox[{
   RowBox[{"Sinh", "[", 
    RowBox[{"h", " ", 
     RowBox[{"\[Omega]", " ", "/", 
      RowBox[{"(", 
       RowBox[{"k", " ", "T"}], ")"}]}]}], "]"}], "/", " ", 
   RowBox[{"(", 
    RowBox[{"1", " ", "+", " ", 
     RowBox[{"Cosh", "[", 
      RowBox[{"h", " ", 
       RowBox[{"\[Omega]", " ", "/", 
        RowBox[{"(", 
         RowBox[{"k", " ", "T"}], ")"}]}]}], "]"}]}], ")"}], " "}]}]], "Input",
 CellChangeTimes->{{3.839308961655654*^9, 3.839309117231615*^9}, {
  3.839309407103361*^9, 
  3.839309420143759*^9}},ExpressionUUID->"75d5f00a-7e16-4e78-95ad-\
d628bc0c567e"],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.8393092936579533`*^9, 
  3.8393092964681726`*^9}},ExpressionUUID->"ea758a36-1cff-4d58-af0d-\
90624df55856"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"\[Sigma]22", " ", "=", " ", 
   RowBox[{"-", "Rs"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"\[Sigma]33", " ", "=", " ", 
   RowBox[{
    RowBox[{
     RowBox[{"-", " ", "30"}], " ", 
     RowBox[{"(", 
      RowBox[{"2", " ", "+", " ", "\[Kappa]"}], ")"}], "Rran"}], " ", "-", 
    RowBox[{
     FractionBox["3", "5"], "Rdd"}]}]}], " ", " ", 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"\[Sigma]34", " ", "=", " ", 
   RowBox[{
    RowBox[{"-", 
     RowBox[{"u\[Beta]", "[", 
      RowBox[{"T", ",", "\[Omega]0"}], "]"}]}], " ", 
    RowBox[{"(", " ", 
     RowBox[{
      RowBox[{
       FractionBox[
        RowBox[{"2", "+", "\[Kappa]"}], 
        RowBox[{"2", " ", 
         RowBox[{"Sqrt", "[", "3", "]"}]}]], "Rran"}], " ", "+", " ", 
      RowBox[{
       FractionBox[
        RowBox[{"Sqrt", "[", "3", "]"}], "10"], "Rdd"}]}], " ", ")"}]}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"\[Sigma]41", " ", "=", " ", 
   RowBox[{
    RowBox[{
     FractionBox["1", 
      RowBox[{"Sqrt", "[", "2", "]"}]], 
     RowBox[{"u\[Beta]", "[", 
      RowBox[{"T", ",", "\[Omega]0"}], "]"}], " ", "Rran"}], " ", "+", " ", 
    RowBox[{
     FractionBox["1", 
      RowBox[{"5", " ", 
       RowBox[{"Sqrt", "[", "2", "]"}]}]], 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"2", " ", 
        RowBox[{"u\[Beta]", "[", 
         RowBox[{"T", ",", 
          RowBox[{"2", "\[Omega]0"}]}], "]"}]}], " ", "+", " ", 
       RowBox[{"u\[Beta]", "[", 
        RowBox[{"T", ",", " ", "\[Omega]0"}], "]"}]}], ")"}], " ", 
     "Rdd"}]}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"\[Sigma]42", " ", "=", " ", 
   RowBox[{
    RowBox[{
     RowBox[{"-", 
      FractionBox["\[Kappa]", 
       RowBox[{"Sqrt", "[", "6", "]"}]]}], 
     RowBox[{"u\[Beta]", "[", 
      RowBox[{"T", ",", " ", "\[Omega]0"}], "]"}], "Rran"}], " ", "-", " ", 
    RowBox[{
     FractionBox["1", 
      RowBox[{"5", " ", 
       RowBox[{"Sqrt", "[", "6", "]"}]}]], 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"2", " ", 
        RowBox[{"u\[Beta]", "[", 
         RowBox[{"T", ",", 
          RowBox[{"2", "\[Omega]0"}]}], "]"}]}], " ", "+", " ", 
       RowBox[{"u\[Beta]", "[", 
        RowBox[{"T", ",", " ", "\[Omega]0"}], "]"}]}], ")"}], "Rdd"}]}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"\[Sigma]43", " ", "=", " ", 
   RowBox[{
    RowBox[{
     FractionBox["\[Kappa]", 
      RowBox[{"2", " ", 
       RowBox[{"Sqrt", "[", "3", "]"}]}]], 
     RowBox[{"u\[Beta]", "[", 
      RowBox[{"T", ",", " ", "\[Omega]0"}], "]"}], " ", "Rran"}], "  ", "-", 
    RowBox[{
     FractionBox["1", 
      RowBox[{"10", " ", 
       RowBox[{"Sqrt", "[", "3", "]"}]}]], " ", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"4", " ", 
        RowBox[{"u\[Beta]", "[", 
         RowBox[{"T", ",", 
          RowBox[{"2", "\[Omega]0"}]}], "]"}]}], " ", "-", " ", 
       RowBox[{"u\[Beta]", "[", 
        RowBox[{"T", ",", " ", "\[Omega]0"}], "]"}]}], ")"}], " ", 
     "Rdd"}]}]}], " ", ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"\[Sigma]44", " ", "=", " ", 
   RowBox[{
    RowBox[{"-", "Rran"}], " ", "-", " ", "Rdd"}]}], 
  ";"}], "\[IndentingNewLine]"}], "Input",
 CellChangeTimes->{{3.8393091358715296`*^9, 3.8393091569283457`*^9}, {
  3.839309193047352*^9, 3.8393092094122753`*^9}, {3.839309266077719*^9, 
  3.8393092733951664`*^9}, {3.839309304357642*^9, 3.8393093956388288`*^9}, {
  3.8393094262690125`*^9, 3.8393095389488087`*^9}, {3.839309595962597*^9, 
  3.839309979715114*^9}, {3.8393102010524263`*^9, 3.8393102562117147`*^9}, {
  3.839310331619531*^9, 3.8393104989410114`*^9}, {3.83931055763206*^9, 
  3.8393106029117975`*^9}},ExpressionUUID->"6af1988c-f593-451c-aee3-\
a866404c8e85"],

Cell[BoxData[
 RowBox[{
  RowBox[{"\[Sigma]eff", " ", "=", " ", 
   RowBox[{
    FractionBox[
     RowBox[{"\[Sigma]43", " ", "\[Sigma]34"}], "\[Sigma]33"], " ", "+", " ", 
    "\[Sigma]44"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.8393105335636373`*^9, 3.839310551233717*^9}, {
  3.839310612804756*^9, 
  3.839310653420809*^9}},ExpressionUUID->"8837b544-b281-4a77-8b46-\
de9d3c1434b0"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"As", " ", "=", " ", 
   RowBox[{
    RowBox[{"-", 
     RowBox[{"Sqrt", "[", "3", "]"}]}], 
    FractionBox["\[Sigma]eff", 
     RowBox[{"\[Sigma]22", " ", "-", " ", "\[Sigma]eff"}]], " ", 
    FractionBox["\[Sigma]42", "\[Sigma]41"]}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"A1", " ", "=", " ", 
   RowBox[{
    FractionBox[
     RowBox[{
      RowBox[{"-", "2"}], " ", "\[Sigma]eff"}], "\[Sigma]41"], 
    RowBox[{"(", 
     RowBox[{
      FractionBox["\[Sigma]41", 
       RowBox[{"2", " ", "\[Sigma]eff"}]], "-", 
      FractionBox[
       RowBox[{
        RowBox[{"Sqrt", "[", "3", "]"}], " ", "\[Sigma]42"}], 
       RowBox[{"2", 
        RowBox[{"(", 
         RowBox[{"\[Sigma]22", "-", "\[Sigma]eff"}], ")"}]}]]}], ")"}]}]}], 
  ";"}]}], "Input",
 CellChangeTimes->{{3.839310656853258*^9, 3.8393107534746046`*^9}, {
  3.8395482628853617`*^9, 3.839548361684517*^9}, {3.8395484150872593`*^9, 
  3.839548433750724*^9}, {3.839548509498388*^9, 3.8395485418616285`*^9}, {
  3.839548861589202*^9, 
  3.8395488617552023`*^9}},ExpressionUUID->"55e2c8fc-b177-40f1-9b08-\
89972828b192"],

Cell["\<\
Let\[CloseCurlyQuote]s check if the limit coincides with my by hand \
calculations\
\>", "Text",
 CellChangeTimes->{{3.8393108021565113`*^9, 
  3.8393108350941086`*^9}},ExpressionUUID->"5293e076-00cf-42eb-adef-\
37ebc2b1a5ff"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Limit", "[", 
  RowBox[{"As", ",", 
   RowBox[{"T", "\[Rule]", "Infinity"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.839310838709202*^9, 
  3.839310857628233*^9}},ExpressionUUID->"1ff8f4c4-2944-4a80-9fa2-\
4b157f5793cf"],

Cell[BoxData[
 RowBox[{"-", 
  FractionBox[
   RowBox[{"Rdd", "+", 
    RowBox[{"Rran", " ", "\[Kappa]"}]}], 
   RowBox[{"Rdd", "+", "Rran", "-", "Rs"}]]}]], "Output",
 CellChangeTimes->{
  3.839310859138202*^9, {3.83939108851354*^9, 3.839391104508295*^9}, 
   3.8395483898377*^9, 
   3.839548469371935*^9},ExpressionUUID->"d059dbf5-2312-4499-9e99-\
398eced822d8"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"AsSeries", "=", 
  RowBox[{
   RowBox[{"Series", "[", 
    RowBox[{"As", ",", 
     RowBox[{"{", 
      RowBox[{"T", ",", "Infinity", ",", "2"}], "}"}]}], "]"}], "//", 
   "Normal"}]}]], "Input",
 CellChangeTimes->{{3.839391436501794*^9, 3.8393914679423313`*^9}, {
  3.839391592773413*^9, 3.8393915951575713`*^9}, {3.8393916555728354`*^9, 
  3.8393916641874776`*^9}},ExpressionUUID->"4fed6489-8dc7-47e8-9fbb-\
b1eec26cb72d"],

Cell[BoxData[
 RowBox[{
  FractionBox[
   RowBox[{
    RowBox[{"-", "Rdd"}], "-", 
    RowBox[{"Rran", " ", "\[Kappa]"}]}], 
   RowBox[{"Rdd", "+", "Rran", "-", "Rs"}]], "+", 
  RowBox[{
   RowBox[{"(", 
    RowBox[{
     RowBox[{"144", " ", 
      SuperscriptBox["h", "2"], " ", 
      SuperscriptBox["Rdd", "3"], " ", "Rran", " ", 
      SuperscriptBox["\[Omega]0", "2"]}], "+", 
     RowBox[{"14544", " ", 
      SuperscriptBox["h", "2"], " ", 
      SuperscriptBox["Rdd", "2"], " ", 
      SuperscriptBox["Rran", "2"], " ", 
      SuperscriptBox["\[Omega]0", "2"]}], "+", 
     RowBox[{"14400", " ", 
      SuperscriptBox["h", "2"], " ", "Rdd", " ", 
      SuperscriptBox["Rran", "3"], " ", 
      SuperscriptBox["\[Omega]0", "2"]}], "+", 
     RowBox[{"21", " ", 
      SuperscriptBox["h", "2"], " ", 
      SuperscriptBox["Rdd", "3"], " ", "Rs", " ", 
      SuperscriptBox["\[Omega]0", "2"]}], "-", 
     RowBox[{"74", " ", 
      SuperscriptBox["h", "2"], " ", 
      SuperscriptBox["Rdd", "2"], " ", "Rran", " ", "Rs", " ", 
      SuperscriptBox["\[Omega]0", "2"]}], "-", 
     RowBox[{"14400", " ", 
      SuperscriptBox["h", "2"], " ", "Rdd", " ", 
      SuperscriptBox["Rran", "2"], " ", "Rs", " ", 
      SuperscriptBox["\[Omega]0", "2"]}], "-", 
     RowBox[{"144", " ", 
      SuperscriptBox["h", "2"], " ", 
      SuperscriptBox["Rdd", "3"], " ", "Rran", " ", "\[Kappa]", " ", 
      SuperscriptBox["\[Omega]0", "2"]}], "-", 
     RowBox[{"7344", " ", 
      SuperscriptBox["h", "2"], " ", 
      SuperscriptBox["Rdd", "2"], " ", 
      SuperscriptBox["Rran", "2"], " ", "\[Kappa]", " ", 
      SuperscriptBox["\[Omega]0", "2"]}], "-", 
     RowBox[{"7200", " ", 
      SuperscriptBox["h", "2"], " ", "Rdd", " ", 
      SuperscriptBox["Rran", "3"], " ", "\[Kappa]", " ", 
      SuperscriptBox["\[Omega]0", "2"]}], "+", 
     RowBox[{"185", " ", 
      SuperscriptBox["h", "2"], " ", 
      SuperscriptBox["Rdd", "2"], " ", "Rran", " ", "Rs", " ", "\[Kappa]", 
      " ", 
      SuperscriptBox["\[Omega]0", "2"]}], "+", 
     RowBox[{"7220", " ", 
      SuperscriptBox["h", "2"], " ", "Rdd", " ", 
      SuperscriptBox["Rran", "2"], " ", "Rs", " ", "\[Kappa]", " ", 
      SuperscriptBox["\[Omega]0", "2"]}], "-", 
     RowBox[{"7200", " ", 
      SuperscriptBox["h", "2"], " ", 
      SuperscriptBox["Rdd", "2"], " ", 
      SuperscriptBox["Rran", "2"], " ", 
      SuperscriptBox["\[Kappa]", "2"], " ", 
      SuperscriptBox["\[Omega]0", "2"]}], "-", 
     RowBox[{"7200", " ", 
      SuperscriptBox["h", "2"], " ", "Rdd", " ", 
      SuperscriptBox["Rran", "3"], " ", 
      SuperscriptBox["\[Kappa]", "2"], " ", 
      SuperscriptBox["\[Omega]0", "2"]}], "+", 
     RowBox[{"7195", " ", 
      SuperscriptBox["h", "2"], " ", "Rdd", " ", 
      SuperscriptBox["Rran", "2"], " ", "Rs", " ", 
      SuperscriptBox["\[Kappa]", "2"], " ", 
      SuperscriptBox["\[Omega]0", "2"]}], "-", 
     RowBox[{"50", " ", 
      SuperscriptBox["h", "2"], " ", 
      SuperscriptBox["Rran", "3"], " ", "Rs", " ", 
      SuperscriptBox["\[Kappa]", "2"], " ", 
      SuperscriptBox["\[Omega]0", "2"]}], "-", 
     RowBox[{"25", " ", 
      SuperscriptBox["h", "2"], " ", 
      SuperscriptBox["Rran", "3"], " ", "Rs", " ", 
      SuperscriptBox["\[Kappa]", "3"], " ", 
      SuperscriptBox["\[Omega]0", "2"]}]}], ")"}], "/", 
   RowBox[{"(", 
    RowBox[{"720", " ", 
     SuperscriptBox["k", "2"], " ", 
     RowBox[{"(", 
      RowBox[{"Rdd", "+", "Rran"}], ")"}], " ", 
     SuperscriptBox[
      RowBox[{"(", 
       RowBox[{"Rdd", "+", "Rran", "-", "Rs"}], ")"}], "2"], " ", 
     SuperscriptBox["T", "2"], " ", 
     RowBox[{"(", 
      RowBox[{"Rdd", "+", 
       RowBox[{"100", " ", "Rran"}], "+", 
       RowBox[{"50", " ", "Rran", " ", "\[Kappa]"}]}], ")"}]}], 
    ")"}]}]}]], "Output",
 CellChangeTimes->{
  3.8393914688505087`*^9, 3.8393915959206314`*^9, {3.8393916581604376`*^9, 
   3.839391665048238*^9}, 3.8395483899333076`*^9, 
   3.839548469469722*^9},ExpressionUUID->"f6a07b24-4afd-403b-a433-\
cf9ff61508f3"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(", 
   RowBox[{
    RowBox[{"(", 
     RowBox[{
      RowBox[{"Coefficient", "[", 
       RowBox[{"AsSeries", ",", "T", ",", " ", "0"}], "]"}], "/.", 
      RowBox[{"\[Kappa]", "\[Rule]", 
       RowBox[{"1", "-", " ", 
        FractionBox["Rs", 
         RowBox[{"2", " ", "Rran"}]]}]}]}], ")"}], "/.", 
    RowBox[{"Rran", "\[Rule]", 
     RowBox[{"R1", "-", "Rdd"}]}]}], ")"}], "//", "Simplify"}]], "Input",
 CellChangeTimes->{{3.839391483831702*^9, 3.8393914935175323`*^9}, {
  3.839391679872758*^9, 3.839391709597688*^9}, {3.8393917613027744`*^9, 
  3.8393917840768366`*^9}, {3.8393925471468725`*^9, 3.839392554166891*^9}, {
  3.839392621629715*^9, 
  3.839392622020747*^9}},ExpressionUUID->"3d00551e-f3a2-4fda-b578-\
5523a701b362"],

Cell[BoxData[
 FractionBox[
  RowBox[{
   RowBox[{
    RowBox[{"-", "2"}], " ", "R1"}], "+", "Rs"}], 
  RowBox[{"2", " ", 
   RowBox[{"(", 
    RowBox[{"R1", "-", "Rs"}], ")"}]}]]], "Output",
 CellChangeTimes->{
  3.8393914941588163`*^9, 3.8393916046469193`*^9, 3.8393916748359203`*^9, 
   3.839391710347625*^9, {3.839391762293975*^9, 3.8393917846450706`*^9}, 
   3.83939255473374*^9, 3.8393926230354013`*^9, 3.8395483899482327`*^9, 
   3.8395484694949136`*^9},ExpressionUUID->"2564b1d0-91c8-4339-9bae-\
315876c00063"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Coefficient", "[", 
  RowBox[{"AsSeries", ",", "T", ",", " ", "1"}], "]"}]], "Input",
 CellChangeTimes->{{3.839391790490498*^9, 
  3.839391790573619*^9}},ExpressionUUID->"30924d22-f4c6-4c88-976d-\
bb91d5fab4d5"],

Cell[BoxData["0"], "Output",
 CellChangeTimes->{3.83939179118246*^9, 3.839548389951335*^9, 
  3.8395484694949136`*^9},ExpressionUUID->"06e4ca44-2154-409c-b20c-\
15aaafe29206"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{
       RowBox[{"Coefficient", "[", 
        RowBox[{"AsSeries", ",", "T", ",", " ", 
         RowBox[{"-", "2"}]}], "]"}], "/.", 
       RowBox[{"\[Kappa]", "\[Rule]", " ", 
        RowBox[{"1", "-", 
         FractionBox["Rs", 
          RowBox[{"2", " ", "Rran"}]]}]}]}], ")"}], "/.", 
     RowBox[{"Rran", "\[Rule]", 
      RowBox[{"R1", "-", "Rdd"}]}]}], ")"}], ")"}], "//", 
  "Simplify"}]], "Input",
 CellChangeTimes->{{3.8393917968867283`*^9, 3.8393918400141926`*^9}, {
  3.839391887027156*^9, 3.8393918942332544`*^9}, {3.839391933724907*^9, 
  3.839391963259228*^9}, {3.8393920143572693`*^9, 3.8393920149119825`*^9}, {
  3.839392411496025*^9, 3.8393925293929644`*^9}, {3.839392631229516*^9, 
  3.8393926753472424`*^9}},ExpressionUUID->"77550271-e972-4dbf-bb11-\
94d187075bf0"],

Cell[BoxData[
 FractionBox[
  RowBox[{
   SuperscriptBox["h", "2"], " ", "Rs", " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{
      RowBox[{"-", "600"}], " ", 
      SuperscriptBox["R1", "3"]}], "+", 
     RowBox[{"20", " ", 
      SuperscriptBox["R1", "2"], " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"4416", " ", "Rdd"}], "+", 
        RowBox[{"35", " ", "Rs"}]}], ")"}]}], "+", 
     RowBox[{"5", " ", "Rs", " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"17280", " ", 
         SuperscriptBox["Rdd", "2"]}], "+", 
        RowBox[{"2928", " ", "Rdd", " ", "Rs"}], "+", 
        RowBox[{"5", " ", 
         SuperscriptBox["Rs", "2"]}]}], ")"}]}], "-", 
     RowBox[{"2", " ", "R1", " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"43488", " ", 
         SuperscriptBox["Rdd", "2"]}], "+", 
        RowBox[{"51120", " ", "Rdd", " ", "Rs"}], "+", 
        RowBox[{"125", " ", 
         SuperscriptBox["Rs", "2"]}]}], ")"}]}]}], ")"}], " ", 
   SuperscriptBox["\[Omega]0", "2"]}], 
  RowBox[{"5760", " ", 
   SuperscriptBox["k", "2"], " ", "R1", " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"150", " ", "R1"}], "-", 
     RowBox[{"149", " ", "Rdd"}], "-", 
     RowBox[{"25", " ", "Rs"}]}], ")"}], " ", 
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{"R1", "-", "Rs"}], ")"}], "2"]}]]], "Output",
 CellChangeTimes->{{3.839391797728115*^9, 3.83939184062241*^9}, {
   3.8393919498483458`*^9, 3.8393919641361012`*^9}, 3.8393924340607724`*^9, 
   3.839392474063505*^9, {3.8393925123682184`*^9, 3.8393925302568054`*^9}, {
   3.83939263311913*^9, 3.8393926769110823`*^9}, 3.8395483900223637`*^9, 
   3.839548469566883*^9},ExpressionUUID->"5b6b013b-6511-4dc8-b04b-\
524432eeeb87"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Coefficient", "[", 
  RowBox[{"AsSeries", ",", "T", ",", " ", 
   RowBox[{"-", "5"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.8393944425211115`*^9, 
  3.8393944468303423`*^9}},ExpressionUUID->"6b2579fe-830d-43e8-9adb-\
012a9b06355a"],

Cell[BoxData["0"], "Output",
 CellChangeTimes->{{3.8393944431754417`*^9, 3.8393944476838465`*^9}, 
   3.839548390030958*^9, 
   3.839548469566883*^9},ExpressionUUID->"3bac33e4-d94a-4741-9f2f-\
e857394c39f1"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(", 
   RowBox[{
    RowBox[{"(", 
     RowBox[{
      RowBox[{"Limit", "[", 
       RowBox[{"A1", ",", 
        RowBox[{"T", "\[Rule]", "Infinity"}]}], "]"}], "/.", 
      RowBox[{"\[Kappa]", "\[Rule]", 
       RowBox[{"1", "-", " ", 
        FractionBox["Rs", 
         RowBox[{"2", " ", "Rran"}]]}]}]}], ")"}], "/.", 
    RowBox[{"Rran", "\[Rule]", 
     RowBox[{"R1", "-", "Rdd"}]}]}], ")"}], "//", "Simplify"}]], "Input",
 CellChangeTimes->{
  3.839548401543191*^9, {3.8395485612345805`*^9, 
   3.839548565160923*^9}},ExpressionUUID->"4c7486ab-2d98-44c9-a8fb-\
a5715122ca79"],

Cell[BoxData[
 FractionBox["Rs", 
  RowBox[{
   RowBox[{"2", " ", "R1"}], "-", 
   RowBox[{"2", " ", "Rs"}]}]]], "Output",
 CellChangeTimes->{
  3.8395484028849416`*^9, 3.8395484696490216`*^9, {3.8395485462646008`*^9, 
   3.8395485668952456`*^9}, 
   3.839548866310707*^9},ExpressionUUID->"f5ecdee0-6014-4a5d-a766-\
5bafef6fc771"]
}, Open  ]],

Cell[BoxData[
 RowBox[{
  RowBox[{"A1Series", "=", 
   RowBox[{
    RowBox[{"Series", "[", 
     RowBox[{"A1", ",", 
      RowBox[{"{", 
       RowBox[{"T", ",", "Infinity", ",", "2"}], "}"}]}], "]"}], "//", 
    "Normal"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.8395488902795744`*^9, 
  3.8395489046646338`*^9}},ExpressionUUID->"ad2da88a-2058-47f3-b6e5-\
ddc1bb97cf6b"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(", 
   RowBox[{
    RowBox[{"(", 
     RowBox[{
      RowBox[{"Coefficient", "[", 
       RowBox[{"A1Series", ",", "T", ",", " ", "0"}], "]"}], "/.", 
      RowBox[{"\[Kappa]", "\[Rule]", 
       RowBox[{"1", "-", " ", 
        FractionBox["Rs", 
         RowBox[{"2", " ", "Rran"}]]}]}]}], ")"}], "/.", 
    RowBox[{"Rran", "\[Rule]", 
     RowBox[{"R1", "-", "Rdd"}]}]}], ")"}], "//", "Simplify"}]], "Input",
 CellChangeTimes->{
  3.8395489140461617`*^9},ExpressionUUID->"07f2ee25-2d52-4eb4-8703-\
2039308c0ba6"],

Cell[BoxData[
 FractionBox["Rs", 
  RowBox[{
   RowBox[{"2", " ", "R1"}], "-", 
   RowBox[{"2", " ", "Rs"}]}]]], "Output",
 CellChangeTimes->{
  3.839548915675701*^9},ExpressionUUID->"033e94e6-d5d4-4d48-ba3e-\
5dcb8bdc3613"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{
       RowBox[{"Coefficient", "[", 
        RowBox[{"A1Series", ",", "T", ",", " ", 
         RowBox[{"-", "2"}]}], "]"}], "/.", 
       RowBox[{"\[Kappa]", "\[Rule]", " ", 
        RowBox[{"1", "-", 
         FractionBox["Rs", 
          RowBox[{"2", " ", "Rran"}]]}]}]}], ")"}], "/.", 
     RowBox[{"Rran", "\[Rule]", 
      RowBox[{"R1", "-", "Rdd"}]}]}], ")"}], ")"}], "//", 
  "Simplify"}]], "Input",
 CellChangeTimes->{
  3.83954892300443*^9},ExpressionUUID->"acbc82ec-cdb0-40fe-b48b-06f4d91c1c4f"],

Cell[BoxData[
 FractionBox[
  RowBox[{
   SuperscriptBox["h", "2"], " ", "Rs", " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"600", " ", 
      SuperscriptBox["R1", "3"]}], "-", 
     RowBox[{"20", " ", 
      SuperscriptBox["R1", "2"], " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"4416", " ", "Rdd"}], "+", 
        RowBox[{"35", " ", "Rs"}]}], ")"}]}], "-", 
     RowBox[{"5", " ", "Rs", " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"17280", " ", 
         SuperscriptBox["Rdd", "2"]}], "+", 
        RowBox[{"2928", " ", "Rdd", " ", "Rs"}], "+", 
        RowBox[{"5", " ", 
         SuperscriptBox["Rs", "2"]}]}], ")"}]}], "+", 
     RowBox[{"2", " ", "R1", " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"43488", " ", 
         SuperscriptBox["Rdd", "2"]}], "+", 
        RowBox[{"51120", " ", "Rdd", " ", "Rs"}], "+", 
        RowBox[{"125", " ", 
         SuperscriptBox["Rs", "2"]}]}], ")"}]}]}], ")"}], " ", 
   SuperscriptBox["\[Omega]0", "2"]}], 
  RowBox[{"5760", " ", 
   SuperscriptBox["k", "2"], " ", "R1", " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"150", " ", "R1"}], "-", 
     RowBox[{"149", " ", "Rdd"}], "-", 
     RowBox[{"25", " ", "Rs"}]}], ")"}], " ", 
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{"R1", "-", "Rs"}], ")"}], "2"]}]]], "Output",
 CellChangeTimes->{
  3.8395489247882233`*^9},ExpressionUUID->"7b58f5dd-7d5f-496e-9259-\
6bd76acfce3f"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"(", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"(", 
       RowBox[{
        RowBox[{"Coefficient", "[", 
         RowBox[{"A1Series", ",", "T", ",", " ", 
          RowBox[{"-", "2"}]}], "]"}], "/.", 
        RowBox[{"\[Kappa]", "\[Rule]", " ", 
         RowBox[{"1", "-", 
          FractionBox["Rs", 
           RowBox[{"2", " ", "Rran"}]]}]}]}], ")"}], "/.", 
      RowBox[{"Rran", "\[Rule]", 
       RowBox[{"R1", "-", "Rdd"}]}]}], ")"}], ")"}], "/", 
   RowBox[{"(", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"(", 
       RowBox[{
        RowBox[{"Coefficient", "[", 
         RowBox[{"AsSeries", ",", "T", ",", " ", 
          RowBox[{"-", "2"}]}], "]"}], "/.", 
        RowBox[{"\[Kappa]", "\[Rule]", " ", 
         RowBox[{"1", "-", 
          FractionBox["Rs", 
           RowBox[{"2", " ", "Rran"}]]}]}]}], ")"}], "/.", 
      RowBox[{"Rran", "\[Rule]", 
       RowBox[{"R1", "-", "Rdd"}]}]}], ")"}], ")"}]}], "//", 
  "Simplify"}]], "Input",
 CellChangeTimes->{{3.8395490594751296`*^9, 
  3.8395490767831774`*^9}},ExpressionUUID->"447b5cb6-c618-448f-9fb5-\
8eaa63ad3c37"],

Cell[BoxData[
 RowBox[{"-", "1"}]], "Output",
 CellChangeTimes->{{3.839549071684655*^9, 
  3.839549077356264*^9}},ExpressionUUID->"1ba532a8-b28f-4416-a891-\
2fd2e58b139f"]
}, Open  ]]
}, Open  ]]
},
WindowSize->{1530, 769},
WindowMargins->{{Automatic, -4}, {Automatic, 2}},
FrontEndVersion->"11.2 for Microsoft Windows (64-bit) (September 10, 2017)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 186, 3, 96, "Title",ExpressionUUID->"c0d3e94b-b71f-4610-8faa-14a51e2f4819"],
Cell[769, 27, 700, 20, 28, "Input",ExpressionUUID->"75d5f00a-7e16-4e78-95ad-d628bc0c567e"],
Cell[1472, 49, 156, 3, 28, "Input",ExpressionUUID->"ea758a36-1cff-4d58-af0d-90624df55856"],
Cell[1631, 54, 3771, 108, 309, "Input",ExpressionUUID->"6af1988c-f593-451c-aee3-a866404c8e85"],
Cell[5405, 164, 388, 10, 51, "Input",ExpressionUUID->"8837b544-b281-4a77-8b46-de9d3c1434b0"],
Cell[5796, 176, 1135, 31, 103, "Input",ExpressionUUID->"55e2c8fc-b177-40f1-9b08-89972828b192"],
Cell[6934, 209, 236, 6, 34, "Text",ExpressionUUID->"5293e076-00cf-42eb-adef-37ebc2b1a5ff"],
Cell[CellGroupData[{
Cell[7195, 219, 245, 6, 28, "Input",ExpressionUUID->"1ff8f4c4-2944-4a80-9fa2-4b157f5793cf"],
Cell[7443, 227, 364, 10, 56, "Output",ExpressionUUID->"d059dbf5-2312-4499-9e99-398eced822d8"]
}, Open  ]],
Cell[CellGroupData[{
Cell[7844, 242, 448, 11, 28, "Input",ExpressionUUID->"4fed6489-8dc7-47e8-9fbb-b1eec26cb72d"],
Cell[8295, 255, 4027, 101, 125, "Output",ExpressionUUID->"f6a07b24-4afd-403b-a433-cf9ff61508f3"]
}, Open  ]],
Cell[CellGroupData[{
Cell[12359, 361, 773, 19, 52, "Input",ExpressionUUID->"3d00551e-f3a2-4fda-b578-5523a701b362"],
Cell[13135, 382, 518, 13, 58, "Output",ExpressionUUID->"2564b1d0-91c8-4339-9bae-315876c00063"]
}, Open  ]],
Cell[CellGroupData[{
Cell[13690, 400, 235, 5, 28, "Input",ExpressionUUID->"30924d22-f4c6-4c88-976d-bb91d5fab4d5"],
Cell[13928, 407, 175, 3, 32, "Output",ExpressionUUID->"06e4ca44-2154-409c-b20c-15aaafe29206"]
}, Open  ]],
Cell[CellGroupData[{
Cell[14140, 415, 888, 22, 52, "Input",ExpressionUUID->"77550271-e972-4dbf-bb11-94d187075bf0"],
Cell[15031, 439, 1718, 47, 63, "Output",ExpressionUUID->"5b6b013b-6511-4dc8-b04b-524432eeeb87"]
}, Open  ]],
Cell[CellGroupData[{
Cell[16786, 491, 258, 6, 28, "Input",ExpressionUUID->"6b2579fe-830d-43e8-9adb-012a9b06355a"],
Cell[17047, 499, 207, 4, 32, "Output",ExpressionUUID->"3bac33e4-d94a-4741-9f2f-e857394c39f1"]
}, Open  ]],
Cell[CellGroupData[{
Cell[17291, 508, 615, 18, 52, "Input",ExpressionUUID->"4c7486ab-2d98-44c9-a8fb-a5715122ca79"],
Cell[17909, 528, 330, 9, 56, "Output",ExpressionUUID->"f5ecdee0-6014-4a5d-a766-5bafef6fc771"]
}, Open  ]],
Cell[18254, 540, 373, 11, 28, "Input",ExpressionUUID->"ad2da88a-2058-47f3-b6e5-ddc1bb97cf6b"],
Cell[CellGroupData[{
Cell[18652, 555, 550, 16, 52, "Input",ExpressionUUID->"07f2ee25-2d52-4eb4-8703-2039308c0ba6"],
Cell[19205, 573, 224, 7, 56, "Output",ExpressionUUID->"033e94e6-d5d4-4d48-ba3e-5dcb8bdc3613"]
}, Open  ]],
Cell[CellGroupData[{
Cell[19466, 585, 608, 18, 52, "Input",ExpressionUUID->"acbc82ec-cdb0-40fe-b48b-06f4d91c1c4f"],
Cell[20077, 605, 1423, 43, 63, "Output",ExpressionUUID->"7b58f5dd-7d5f-496e-9259-6bd76acfce3f"]
}, Open  ]],
Cell[CellGroupData[{
Cell[21537, 653, 1139, 34, 52, "Input",ExpressionUUID->"447b5cb6-c618-448f-9fb5-8eaa63ad3c37"],
Cell[22679, 689, 171, 4, 65, "Output",ExpressionUUID->"1ba532a8-b28f-4416-a891-2fd2e58b139f"]
}, Open  ]]
}, Open  ]]
}
]
*)

