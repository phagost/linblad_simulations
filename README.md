# Supplementary data for mr-2021-62 article

This is a supplementary repository for the ["Spin relaxation: under the sun, anything new?"](https://doi.org/10.5194/mr-2021-62) article. This simulation data was made without the thought for public sharing so it is lacking in comments and good structure.

If something is unclear (which is quite probable), one may contact either Daniel Abergel or Bogdan Rodin for additional explanations.

## Files content

* `2_spin_relax_matrix_linblad.nb`

    This file contains preliminary derivation for relaxation matrix for 2-spin system where DD and partially correlated field are the main sources of relaxation. The beautiful [SpinDynamica](http://www.spindynamica.soton.ac.uk/) package is needed for the file proper use.

*  `expansion_of_as_and_a1.nb`

    This file contains some information about the expansion for coefficients in the singlet order relaxation final equation.

* `relaxation_scripts_scilab.zip`

    This group of files contains the code for numerical simulations.
  
